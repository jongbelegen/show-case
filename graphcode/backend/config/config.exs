# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

# Configures the endpoint
config :graphcode, GraphcodeWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "1X7jIT6TtQ7sdlpe/x/GVuKoLaEmCO4/+QfOEZ8h2JOPVRIAkDMbLvFt7bhwqqur",
  render_errors: [view: GraphcodeWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Graphcode.PubSub, adapter: Phoenix.PubSub.PG2],
  live_view: [signing_salt: "z85XcV2g"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :ex_aws,
  json_codec: Jason

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :graphcode, Graphcode.Guardian,
       issuer: "graphcode",
       secret_key: "3gKE7e5ohkWBfpuUf1GY+7rpeWsUg4qZCypM3UFM81O1wkAgYcJ2JALu7YfNFhKi"

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
