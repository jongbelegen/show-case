defmodule Graphcode.Repo do
  use Ecto.Repo,
      otp_app: :graphcode,
      adapter: Ecto.Adapters.Postgres
end
