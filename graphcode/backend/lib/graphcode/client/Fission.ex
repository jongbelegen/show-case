defmodule Graphcode.Client.Fission do
  use HTTPoison.Base

  @endpoint "http://127.0.0.1:8080/v2/"

  def process_url(url) do
    @endpoint <> url
  end

  def process_request_body(body) do
    Jason.encode!(body)
  end

  def process_response_body(body) do
    Jason.decode(body)
  end
end
