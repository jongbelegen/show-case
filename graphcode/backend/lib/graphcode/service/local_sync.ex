defmodule Graphcode.Service.LocalSync do
  alias Graphcode.Service.Spec
  alias GraphcodeWeb.Uploaders.FunctionSrc
  alias Graphcode.Utils.Hashing
  alias Graphcode.Service.Func

  def start do
    specs = Spec.collect("poll-maker")

    specs =
      Enum.map(specs, fn spec ->
        {:ok, path} = FunctionSrc.store(spec.function_source_path)
        function_stored_location = FunctionSrc.url(path)
        file_hash = Hashing.hash_file(spec.function_source_path)

        spec =
          Map.merge(spec, %{
            function_stored_location: function_stored_location,
            file_hash: file_hash
          })

        Func.create(spec)
      end)

    IO.inspect(specs)
  end
end
