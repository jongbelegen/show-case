defmodule Graphcode.Service.Func.Package do
  alias Graphcode.Client.Fission

  def create(spec) do
    {:ok, resp} =
      Fission.post!("packages", %{
        metadata: %{
          name: spec["id"],
          namespace: "default"
        },
        spec: %{
          deployment: %{
            type: "url",
            url:
              spec.function_stored_location,
            checksum: %{
              sum: spec.file_hash,
              type: "sha256"
            }
          },
          environment: %{
            name: "nodejs",
            namespace: "default"
          }
        }
      }).body

    resp
  end

  def delete(name) do
    Fission.delete!("packages/#{name}")
  end
end
