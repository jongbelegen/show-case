defmodule Graphcode.Service.Spec do
  @base_path "priv/plugins"

  def collect(plugin_name) do
    with true <- File.exists?("#{@base_path}/#{plugin_name}"),
         {:ok, spec_list} <- get_spec_paths(plugin_name) do
      Enum.map(spec_list, &path_to_struct/1)
    else
      false -> {:error, :plugin_cannot_be_found}
      {:error, msg} -> {:error, msg}
    end
  end

  defp get_spec_paths(plugin_name) do
    case Path.wildcard("#{@base_path}/#{plugin_name}/**/spec.json") do
      [] -> {:error, :cannot_find_spec_files}
      specs -> {:ok, specs}
    end
  end

  defp path_to_struct(path) do
    with {:ok, file} <- File.read(path),
         {:ok, spec} <- Jason.decode(file) do
      find_source_file_path(spec, path)
    else
      {:error, msg} -> {:error, msg}
    end
  end

  defp find_source_file_path(%{"type" => "function"} = spec, path) do
    dir = Path.dirname(path)

    spec |> Map.put(:function_source_path, "#{dir}/#{spec["entry"]}")
  end

  defp find_source_file_path(spec, _path), do: spec

end
