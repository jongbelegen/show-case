defmodule Graphcode.Service.Func do
  alias Graphcode.Client.Fission
  alias Graphcode.Service.Func.Package

  def list do
    Fission.get!("functions")
  end

  def create(spec) do
    package = Package.create(spec)
    IO.inspect(package)

    Fission.post!("functions", %{
      metadata: %{
        name: spec["name"],
        namespace: "default"
      },
      spec: %{
        environment: %{
          name: "nodejs",
          namespace: "default"
        },
        package: %{
          packageref: %{
            name: package["name"],
            namespace: package["namespace"],
            resourceVersion: package["resourceVersion"]
          }
        },
        InvokeStrategy: %{
          ExecutionStrategy: %{
            ExecutorType: "poolmgr",
            SpecializationTimeout: 120
          },
          StrategyType: "execution"
        },
        functionTimeout: 60
      }
    })
  end
end
