defmodule Graphcode.Account do
  @moduledoc """
  Module that takes care of account related actions
  """

  alias Graphcode.Account.User
  alias Graphcode.Account.Company
  alias Graphcode.Repo
  alias Graphcode.Guardian

  @spec create_user_with_company(
          %{email: String.t(), first_name: String.t(), last_name: String.t()},
          String.t()
        ) :: :ok
  def create_user_with_company(user, company_name) do
    company_changeset = Company.changeset(%Company{}, %{name: company_name})

    {:ok, user} =
      User.changeset(%User{}, user)
      |> Ecto.Changeset.put_assoc(:companies, [company_changeset])
      |> Repo.insert()


    {:ok, token, _claims} = Guardian.encode_and_sign(user)
    user = user |> Map.put(:auth_token, token)
    {:ok, user}
  end

  def get_user_by_id(id) do
    Repo.get(User, id)
  end
end
