defmodule GraphcodeWeb.Resolvers.User do
  alias Graphcode.Repo
  alias Graphcode.Account

  def create(
        _parent,
        %{
          company: company,
          email: email,
          first_name: first_name,
          last_name: last_name,
          password: password
        },
        _resolution
      ) do
    Account.create_user_with_company(
      %{
        email: email,
        first_name: first_name,
        last_name: last_name,
        password: password,
        last_name: last_name
      },
      company
    )
  end
end
