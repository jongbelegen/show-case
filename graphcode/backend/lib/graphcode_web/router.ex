defmodule GraphcodeWeb.Router do
  use GraphcodeWeb, :router

  pipeline :api do
    plug(:fetch_session)
    plug(:accepts, ["json"])

    plug(Absinthe.Plug,
      schema: GraphcodeWeb.Schema,
      before_send: {__MODULE__, :absinthe_before_send}
    )
  end

  scope "/api" do
    pipe_through(:api)

    forward("/graphiql", Absinthe.Plug.GraphiQL, schema: GraphcodeWeb.Schema)
    forward("/", Absinthe.Plug, schema: GraphcodeWeb.Schema)
  end

  def absinthe_before_send(conn, %Absinthe.Blueprint{} = blueprint) do
    if auth_token = blueprint.execution.context[:auth_token] do
      put_resp_cookie(conn, "auth_token", auth_token)
    else
      conn
    end
  end

  def absinthe_before_send(conn, _) do
    conn
  end
end
