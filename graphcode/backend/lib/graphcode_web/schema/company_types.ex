defmodule GraphcodeWeb.Schema.CompanyTypes do
  use Absinthe.Schema.Notation

  object :company do
    field :id, :id
    field :name, :string
  end
end
