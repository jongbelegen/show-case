defmodule GraphcodeWeb.PageController do
  use GraphcodeWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
