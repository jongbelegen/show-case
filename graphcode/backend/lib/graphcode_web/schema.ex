defmodule GraphcodeWeb.Schema do
  use Absinthe.Schema

  import_types(GraphcodeWeb.Schema.UserTypes)
  import_types(GraphcodeWeb.Schema.CompanyTypes)

  alias GraphcodeWeb.Resolvers

  query do
    @desc "Get all posts"
    field :company, :company do
      resolve(&Resolvers.Company.show/3)
    end
  end

  mutation do
    field :create_user, :user do
      arg(:first_name, non_null(:string))
      arg(:last_name, non_null(:string))
      arg(:email, non_null(:string))
      arg(:password, non_null(:string))
      arg(:company, non_null(:string))

      resolve(&Resolvers.User.create/3)

      middleware &set_auth_cookie/2
    end
  end

  def set_auth_cookie(resolution, _) do
    with %{value: %{auth_token: auth_token}} <- resolution do
      Map.update!(resolution, :context, fn ctx ->
        Map.put(ctx, :auth_token, auth_token)
      end)
    end
  end
end
