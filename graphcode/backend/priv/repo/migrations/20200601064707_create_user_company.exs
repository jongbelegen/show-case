defmodule Graphcode.Repo.Migrations.CreateUserCompany do
  use Ecto.Migration

  def change do
    create table(:user_company) do
      add :user_id, references(:users)
      add :company_id, references(:companies)
    end

    create unique_index(:user_company, [:user_id, :company_id])
  end
end
