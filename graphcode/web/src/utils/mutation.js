import gql from "graphql-tag";

export const createUser = gql`
  mutation createUser(
    $company: String!
    $firstName: String!
    $lastName: String!
    $password: String!
    $email: String!
  ) {
    createUser(
      company: $company
      password: $password
      firstName: $firstName
      lastName: $lastName
      email: $email
    ) {
      email
      id
      first_name
      last_name
    }
  }
`;
