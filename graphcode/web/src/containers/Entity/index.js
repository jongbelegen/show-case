import React, { useState } from "react";
import PropTypes from "prop-types";
import { Button, Card, Elevation, EditableText, H1 } from "@blueprintjs/core";
import { FormBuilder } from "../../components/FormBuilder";

export function Entity() {
  const [input, setInput] = useState({
    title: "",
    description: "",
  });
  const handleChange = name => value => {
    setInput({
      ...input,
      [name]: value,
    });
  };

  return (
    <>
      <H1>
        <EditableText
          onChange={handleChange('title')}
          name="title"
          maxLength="255"
          placeholder="Edit title..."
        />
      </H1>
      <FormBuilder/>
    </>
  );
}

Entity.propTypes = {};
