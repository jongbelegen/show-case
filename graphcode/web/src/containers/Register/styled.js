import styled from "@emotion/styled";
import { Card as CardRaw } from "@blueprintjs/core";

export const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  min-height: 100vh;
`

export const Card = styled(CardRaw)`
  width: 400px;
`
