import React, { useState } from "react";
import PropTypes from "prop-types";
import { Wrapper, Card } from "./styled";
import {
  Elevation,
  FormGroup,
  InputGroup,
  H4,
  Button,
} from "@blueprintjs/core";
import { useMutation } from "@apollo/react-hooks";
import * as mutation from "../../utils/mutation";

export function Register() {
  const [input, setInput] = useState({
    company: "",
    password: "",
    email: "",
    firstName: "",
    lastName: "",
  });

  const [createUser] = useMutation(mutation.createUser, {
    variables: input,
  });

  const handleChange = ({ target }) => {
    setInput({
      ...input,
      [target.name]: target.value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    createUser();
  };

  return (
    <Wrapper>
      <Card elevation={Elevation.ONE}>
        <H4>Register</H4>
        <form onSubmit={handleSubmit}>
          <FormGroup label="Company" labelInfo="(required)">
            <InputGroup
              name="company"
              onChange={handleChange}
              placeholder="Google"
            />
          </FormGroup>

          <FormGroup label="First name" labelInfo="(required)">
            <InputGroup name="firstName" onChange={handleChange} />
          </FormGroup>

          <FormGroup label="Last name" labelInfo="(required)">
            <InputGroup name="lastName" onChange={handleChange} />
          </FormGroup>

          <FormGroup label="Email" labelInfo="(required)">
            <InputGroup type="email" name="email" onChange={handleChange} />
          </FormGroup>

          <FormGroup label="Password" labelInfo="(required)">
            <InputGroup
              type="password"
              name="password"
              onChange={handleChange}
            />
          </FormGroup>
          <Button type="submit">Submit</Button>
        </form>
      </Card>
    </Wrapper>
  );
}

Register.propTypes = {};
