import styled from "@emotion/styled";
import { Colors } from "@blueprintjs/core";

export const Root = styled.div`
  overflow: hidden;
  height: 100vh;
  padding: 2rem;
`;
