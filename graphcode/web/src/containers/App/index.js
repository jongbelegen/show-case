import React from "react";

import "normalize.css";
import "@blueprintjs/core/lib/css/blueprint.css";
import { Root } from "./styled";
import * as ROUTES from "../../utils/routes";
import { Register } from "../Register";
import { Entity } from "../Entity";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { ApolloProvider } from "@apollo/react-hooks";
import { client } from "../../utils/apollo";

function App() {
  return (
    <ApolloProvider client={client}>
      <Root>
        <Router>
          <Switch>
            <Route path={ROUTES.REGISTER} component={Register} />
            <Route path={ROUTES.ENTITY} component={Entity} />
          </Switch>
        </Router>
      </Root>
    </ApolloProvider>
  );
}

export default App;
