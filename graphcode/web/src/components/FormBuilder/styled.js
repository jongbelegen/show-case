import styled from "@emotion/styled";
import { Colors } from "@blueprintjs/core";

export const Wrapper = styled.div`
  display: flex;
`;

export const CanvasWrapper = styled.div`
  margin-left: 3rem;
  width: 100%;
  border: dashed 6px ${Colors.GRAY1};
  min-height: 500px;
`;
