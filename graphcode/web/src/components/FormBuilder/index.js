import React from "react";
import { Wrapper, CanvasWrapper } from "./styled";
import { Editor, Frame, Canvas, useEditor } from "@craftjs/core";
import { FormBuilderToolBox, tools } from "../FormBuilderToolBox";
import { Draggable } from "../FormBuilderToolBox/Draggable";
import { FormBuilderSettings } from "../FormBuilderSettings";
import { DroppableRegion } from "../DroppableRegion";

const inUseComponents = tools.reduce((prev, curr) => {
  return {
    ...prev,
    [curr.id]: curr.Component,
  };
});

const renderNode = ({ render }) => {
  if (render.type === Canvas) {
    return render;
  }
  return <Draggable>{render}</Draggable>;
};

export function FormBuilder() {
  return (
    <Editor resolver={inUseComponents} onRender={renderNode}>
      <Wrapper>
        <FormBuilderToolBox />
        <CanvasWrapper>
          <Frame>
            <Canvas is={DroppableRegion}></Canvas>
          </Frame>
        </CanvasWrapper>
        <FormBuilderSettings />
      </Wrapper>
    </Editor>
  );
}

FormBuilder.propTypes = {};
