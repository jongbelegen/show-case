import React from "react";
import PropTypes from "prop-types";
import { InputGroup, FormGroup, H3 } from "@blueprintjs/core";
import { Draggable } from "../Draggable";
import { uniqueId } from "../../../utils/string";

export function TextInput({ label, helperText, labelInfo, placeholder }) {
  const id = uniqueId();

  return (
    <FormGroup
      helperText={helperText}
      label={label}
      labelFor={id}
      labelInfo={labelInfo}
    >
      <InputGroup id={id} placeholder={placeholder} readOnly />
    </FormGroup>
  );
}

TextInput.craft = {
  defaultProps: {
    label: "Label",
    helperText: "",
    labelInfo: "",
    placeholder: "",
  },
};

TextInput.settings = [
  {
    key: "label",
    label: "Label",
    type: "string",
  },
  {
    key: "helperText",
    label: "Helper text",
    type: "string",
  },
  {
    key: "labelInfo",
    label: "Label Info",
    type: "string",
  },
  {
    key: "placeholder",
    label: "Placeholder",
    type: "string",
  },
];

export const Preview = () => {
  return <h3>Text</h3>;
};
