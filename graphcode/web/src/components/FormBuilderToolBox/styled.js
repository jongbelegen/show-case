import styled from "@emotion/styled";
import { Colors } from "@blueprintjs/core";

export const NoInteractions = styled.div`
  position: relative;
  border: 2px solid ${Colors.GRAY1};
  border-radius: 3px;
  padding: 1rem;
  text-align: center;
  margin-bottom: 1rem;

  &:before {
    z-index: 2;
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    content: "";
  }
`;
