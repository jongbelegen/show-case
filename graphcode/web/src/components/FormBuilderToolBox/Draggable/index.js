import React from "react";
import PropTypes from "prop-types";
import { useNode } from "@craftjs/core";
import { Wrapper } from "./styled";

export function Draggable({ className, children }) {
  const {
    connectors: { connect, drag, selected },
  } = useNode((state) => {
    return {
      selected: state.events.selected,
    };
  });

  return (
    <Wrapper className={className} ref={(ref) => connect(drag(ref))}>
      {children}
    </Wrapper>
  );
}

Draggable.propTypes = {};
