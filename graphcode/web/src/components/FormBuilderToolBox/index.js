import React from "react";
import PropTypes from "prop-types";
import { useEditor } from "@craftjs/core";
import { Card, H3, H5, InputGroup } from "@blueprintjs/core";
import { TextInput, Preview as TextInputPreview } from "./TextInput";
import { NumberInput, Preview as NumberInputPreview } from "./NumberInput";
import { FileInput, Preview as FileInputPreview } from "./FileInput";
import { Heading, Preview as HeadingPreview } from "./Heading";
import { Grid, Preview as GridPreview } from "./Grid";
import { NoInteractions } from "./styled";

export const tools = [
  {
    id: "TextInput",
    Component: TextInput,
    Preview: TextInputPreview,
  },
  {
    id: "NumberInput",
    Component: NumberInput,
    Preview: NumberInputPreview,
  },
  {
    id: "FileInput",
    Component: FileInput,
    Preview: FileInputPreview,
  },
  {
    id: "Heading",
    Component: Heading,
    Preview: HeadingPreview,
  },
  {
    id: "Grid",
    Component: Grid,
    Preview: GridPreview,
  },
];

export function FormBuilderToolBox() {
  const {
    connectors: { create },
    query,
    actions,
  } = useEditor();

  window.query = query;
  window.actions = actions;

  return (
    <div>
      <H3>Toolbox</H3>
      {Object.keys(tools).map((tool) => {
        const { Component, Preview } = tools[tool];

        return (
          <NoInteractions key={tool} ref={(ref) => create(ref, <Component />)}>
            <Preview />
          </NoInteractions>
        );
      })}
    </div>
  );
}

FormBuilderToolBox.propTypes = {};
