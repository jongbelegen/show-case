import React from "react";
import PropTypes from "prop-types";
import { FileInput as BluePrintFileInput, FormGroup } from "@blueprintjs/core";
import { Draggable } from "../Draggable";
import { uniqueId } from "../../../utils/string";

export function FileInput({ label, helperText, labelInfo, placeholder }) {
  const id = uniqueId();

  return (
    <FormGroup
      labelFor={id}
      helperText={helperText}
      label={label}
      labelInfo={labelInfo}
    >
      <BluePrintFileInput
        fill
        id={id}
        placeholder="Placeholder text"
        readOnly
      />
    </FormGroup>
  );
}

FileInput.craft = {
  defaultProps: {
    label: "Label",
    helperText: "",
    labelInfo: "",
    placeholder: "",
  },
};

FileInput.settings = [
  {
    key: "label",
    label: "Label",
    type: "string",
  },
  {
    key: "helperText",
    label: "Helper text",
    type: "string",
  },
  {
    key: "labelInfo",
    label: "Label Info",
    type: "string",
  },
  {
    key: "placeholder",
    label: "Placeholder",
    type: "string",
  },
];

export const Preview = () => {
  return <h3>File</h3>;
};
