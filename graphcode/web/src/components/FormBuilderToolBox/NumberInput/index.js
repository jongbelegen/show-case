import React from "react";
import { NumericInput, FormGroup } from "@blueprintjs/core";
import { uniqueId } from "../../../utils/string";

export function NumberInput({ label, helperText, labelInfo, placeholder }) {
  const id = uniqueId();

  return (
    <FormGroup
      helperText={helperText}
      label={label}
      labelFor={id}
      labelInfo={labelInfo}
    >
      <NumericInput fill id={id} placeholder={placeholder} readOnly />
    </FormGroup>
  );
}

NumberInput.craft = {
  defaultProps: {
    label: "Label",
    helperText: "",
    labelInfo: "",
    placeholder: "",
  },
};

NumberInput.settings = [
  {
    key: "label",
    label: "Label",
    type: "string",
  },
  {
    key: "helperText",
    label: "Helper text",
    type: "string",
  },
  {
    key: "labelInfo",
    label: "Label Info",
    type: "string",
  },
  {
    key: "placeholder",
    label: "Placeholder",
    type: "string",
  },
];

export const Preview = () => {
  return <h3>Number</h3>;
};
