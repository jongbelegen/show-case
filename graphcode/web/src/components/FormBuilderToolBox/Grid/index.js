import React from "react";
import { Row, Col } from "react-simple-flex-grid";
import { Canvas } from "@craftjs/core";
import { DroppableRegion } from "../../DroppableRegion";
import { uniqueId } from "../../../utils/string";

export function Grid() {
  return (
    <Row gutter={20}>
      <Col span={6}>
        <Canvas id={uniqueId()} is={DroppableRegion} />
      </Col>
      <Col span={6}>
        <Canvas id={uniqueId()} is={DroppableRegion} />
      </Col>
    </Row>
  );
}

Grid.settings = [];

export const Preview = () => {
  return <h3>Columns</h3>;
};
