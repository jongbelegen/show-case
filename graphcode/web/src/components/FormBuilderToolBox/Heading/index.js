import React from "react";
import { InputGroup, FormGroup, H3, H4, H5 } from "@blueprintjs/core";
import { Draggable } from "../Draggable";
import { uniqueId } from "../../../utils/string";

export function Heading({ size, content }) {
  console.log(size);
  const Component = {
    sm: H5,
    md: H4,
    lg: H3,
  }[size];

  return <Component>{content}</Component>;
}

Heading.craft = {
  defaultProps: {
    content: "Heading",
    size: "md",
  },
};

Heading.settings = [
  {
    key: "content",
    label: "Content",
    type: "string",
  },
  {
    key: "size",
    label: "Size",
    type: "option",
    options: [
      {
        value: "sm",
        label: "Small",
      },
      {
        value: "md",
        label: "Medium",
      },
      {
        value: "lg",
        label: "Large",
      },
    ],
  },
];

export const Preview = () => {
  return <h3>Heading</h3>;
};
