import React from "react";
import PropTypes from "prop-types";
import { H4 } from "@blueprintjs/core";
import { Wrapper } from "./styled";
import { useEditor } from "@craftjs/core";
import { FormGroup, InputGroup } from "@blueprintjs/core";
import { Menu } from "../Menu";

export function FormBuilderSettings() {
  const {
    activeNode,
    settings,
    nodeProps,
    actions: { setProp },
  } = useEditor((state) => {
    const activeNode = state.nodes[state.events.selected];
    return {
      activeNode,
      settings: activeNode && activeNode.data.type.settings,
      nodeProps: activeNode && activeNode.data.props,
    };
  });

  const handleChange = ({ target: { name, value } }) => {
    setProp(activeNode.id, (props) => {
      props[name] = value;
    });
  };

  const onSelect = (key) => ({ value }) => {
    setProp(activeNode.id, (props) => {
      props[key] = value;
    });
  };

  return (
    <Wrapper>
      <H4>Options</H4>
      {settings &&
        settings.map(({ key, label, type, options }) => {
          if (type === "option") {
            const currentValue = nodeProps[key];
            const option = options.find((o) => o.value === currentValue);
            return (
              <Menu
                items={options}
                onSelect={onSelect(key)}
                option={option}
                label={label}
              />
            );
          }

          return (
            <FormGroup key={activeNode.id + key} label={label} labelFor={key}>
              <InputGroup
                onChange={handleChange}
                name={key}
                id={key}
                value={nodeProps[key]}
                placeholder="Placeholder text"
              />
            </FormGroup>
          );
        })}
    </Wrapper>
  );
}

FormBuilderSettings.propTypes = {};
