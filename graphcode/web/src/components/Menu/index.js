import React from "react";
import { Button, Classes, FormGroup, MenuItem } from "@blueprintjs/core";
import { ItemRenderer, ItemPredicate, Select } from "@blueprintjs/select";

const renderFilm = (item, { handleClick, modifiers }) => {
  if (!modifiers.matchesPredicate) {
    return null;
  }
  return (
    <MenuItem
      active={modifiers.active}
      key={item.value}
      onClick={handleClick}
      text={item.label}
    />
  );
};

const isItemEqual = (a, b) => {
  return a.value === b.value;
};

export function Menu({ items, onSelect, option, label }) {
  return (
    <FormGroup label={label}>
      <Select
        itemRenderer={renderFilm}
        items={items}
        onItemSelect={onSelect}
        activeItem={option}
        itemsEqual={isItemEqual}
        filterable={false}
      >
        <Button fill text={option.label} rightIcon="double-caret-vertical" />
      </Select>
    </FormGroup>
  );
}

Menu.propTypes = {};
