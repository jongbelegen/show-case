import styled from "@emotion/styled";

export const Wrapper = styled.div`
  padding: ${({ root }) => root && "1rem"};
  min-height: ${({ root }) => root && "400px"};
`;

export const DropHere = styled.div`
  text-align: center;
`;
