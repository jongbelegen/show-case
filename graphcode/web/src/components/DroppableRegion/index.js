import React from "react";
import PropTypes from "prop-types";
import { useNode } from "@craftjs/core";
import { Wrapper, DropHere } from "./styled";

export function DroppableRegion({ children }) {
  const {
    id,
    connectors: { connect, drag },
    childNodeCount,
  } = useNode((node) => {
    console.log(node);
    return { childNodeCount: node?.data?.nodes?.length };
  });

  return (
    <Wrapper root={id === "canvas-ROOT"} ref={(ref) => connect(drag(ref))}>
      {childNodeCount ? children : <DropHere>PLACE ELEMENT HERE</DropHere>}
    </Wrapper>
  );
}

DroppableRegion.craft = {
  rules: {
    canDrag: false,
  },
};
